#!/bin/bash
while getopts "t:" OPTION
do
	case $OPTION in
		t)
			TAG=$OPTARG
			
			docker build -t palisade-development .
			docker run -dit --name palisade -p 80:80 palisade-development
			docker exec -it palisade sh -c "cd palisade-release; git checkout tags/$TAG; exit;"
			docker commit palisade palisade:$TAG
			docker login registry.gitlab.com
			docker image tag palisade:$TAG registry.gitlab.com/palisade/palisade-development/$TAG:$TAG
			docker image tag palisade:$TAG registry.gitlab.com/palisade/palisade-development/$TAG:latest
			docker push registry.gitlab.com/palisade/palisade-development/$TAG:$TAG
			docker push registry.gitlab.com/palisade/palisade-development/$TAG:latest
			exit
			;;
		\?)
			echo "Please use the -t flag to set a tag"
			exit
			;;
	esac
done

